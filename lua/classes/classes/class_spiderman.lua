CLASS.AddClass("SPIDERMAN", {
    color = Color(209, 47, 23, 255),
    passiveItems = {
        "item_ttt_nofalldmg"
    },
    passiveWeapons = {
        "grapplehook" -- TTT Grapplehook
    },
    lang = {
        name = {
            English = "Spiderman",
            Deutsch = "Spiderman"
        },
        desc = {
            English = "Pizza time!",
            Deutsch = "Pizza time!"
        }
    }
})
